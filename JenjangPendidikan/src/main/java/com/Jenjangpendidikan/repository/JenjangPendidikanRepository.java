package com.Jenjangpendidikan.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.Jenjangpendidikan.model.JenjangPendidikan;


public interface JenjangPendidikanRepository extends JpaRepository<JenjangPendidikan, Long> {
	List<JenjangPendidikan> findByIsDelete(Boolean isDelete);
	@Query(value = "FROM JenjangPendidikan WHERE name =?1 AND  isDelete=false")
	List<JenjangPendidikan> findByName(String name);
//    @Query("FROM JenjangPendidikan WHERE lower(name) LIKE lower(concat('%',?1,'%')) order by id asc")
 //   List<JenjangPendidikan> SearchJenjangPendidikan(String keyword);
//    @Query("SELECT c FROM JenjangPendidikan c WHERE name = ?1 AND isDelete = false")
//    Optional<JenjangPendidikan> findByName(String name);
	@Query(value = "FROM JenjangPendidikan WHERE Lower(name) LIKE Lower(concat('%', ?1 , '%')) AND isDelete=false")
	List<JenjangPendidikan> searchByKeyword(String keyword);
	
	
	
}
