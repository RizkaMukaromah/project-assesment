package com.Jenjangpendidikan.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Jenjangpendidikan.model.Role;



public interface RoleRepository extends JpaRepository<Role, Long> {
	List<Role> findByIsDelete(Boolean isDelete);

}
