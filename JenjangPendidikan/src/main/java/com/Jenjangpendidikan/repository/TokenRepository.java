package com.Jenjangpendidikan.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.Jenjangpendidikan.model.Token;



public interface TokenRepository extends JpaRepository<Token, Long>{

	List<Token> findByToken(String token);
//	List<Token> findByEmail(String email);
	
	List<Token> findByIsExpired(Boolean isExpired);
	
	@Query(value="SELECT MAX(id) FROM Token")
	List<Token> findByMaxId(long id);

}

