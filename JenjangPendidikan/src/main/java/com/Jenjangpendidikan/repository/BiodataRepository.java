package com.Jenjangpendidikan.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.Jenjangpendidikan.model.Biodata;



public interface BiodataRepository extends JpaRepository<Biodata, Long> {
	List<Biodata> findByIsDelete(Boolean isDelete);
	
	@Query(value ="SELECT MAX(id) FROM Biodata")
	public Long findByMaxId();

	
	
}
