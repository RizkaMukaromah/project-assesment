package com.Jenjangpendidikan.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.Jenjangpendidikan.model.Pendaftaran;



public interface PendaftaranRepository extends JpaRepository<Pendaftaran, Long> {
	List<Pendaftaran> findByEmail(String email);

	
	

}
