package com.Jenjangpendidikan.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.Jenjangpendidikan.repository.PendaftaranRepository;



@Controller
@RequestMapping("/pendaftaran/")
public class PendaftaranController {

	@Autowired
	private PendaftaranRepository pendaftaranRepository;
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("pendaftaran/indexapi");
		return view;
	}
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("pendaftaran/index");
		return view;
	}
	
	@GetMapping("lupa")
	public ModelAndView lupa() {
		ModelAndView view = new ModelAndView("pendaftaran/lupa");
		return view;
	}
}


