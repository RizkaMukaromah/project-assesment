package com.Jenjangpendidikan.controller;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;

import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/kirimemail")
public class KirimEmailController {
	 public static String from, to, sub, host, content, OTP;
	    
	 @RequestMapping(method = RequestMethod.POST)
	    public String SendEmail(HttpServletRequest request){
	        from = "rizkaocva@gmail.com";

	        //to = request.getParameter("email");
	        to = "rizkaocva24@gmail.com";
	        host = "localhost";
	        sub = "OTP pendaftaran";
	        content = "Hi Bestie,"
	                + "Berikut OTP untuk melakukan registrasi."
	                + "OTP ini akan berakhir dalam 10 Menit.";
	        
	        Properties p = new Properties();
	        p.put("mail.smtp.auth","true");
	        p.put("mail.smtp.starttls.enable","true");
	        p.put("mail.smtp.host","smtp.gmail.com");
	        p.put("mail.smtp.port","587");
	        
	         Session s = Session.getDefaultInstance(p, new javax.mail.Authenticator() {
	             protected PasswordAuthentication getPasswordAuthentication(){
	                 return new PasswordAuthentication("rizkaocva@gmail.com","ztgntkmrutxxwgnq");
	             }
	         });
	         
	         try{
	             MimeMessage m = new MimeMessage(s);
	             try{
	                  m.setFrom(from);
	                  m.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
	                  m.setSubject(sub);
	                  
	                  Multipart mp = new MimeMultipart();
	                  
	                  MimeBodyPart textBodyPart = new MimeBodyPart();
	                  textBodyPart.setText(content);
	                  
	                  mp.addBodyPart(textBodyPart);
	                      
	                  m.setContent(mp);
	                  Transport.send(m);
	                  System.out.println("Sending Message Success");
	                  return "Send Email Success";
	             }catch(Exception e){
	                 e.printStackTrace();
	                 return "Send Email Success";
	             }
	         }catch(Exception e){
	             e.printStackTrace();
	             return "Send Email Success";
	         }
	    }
	    

}