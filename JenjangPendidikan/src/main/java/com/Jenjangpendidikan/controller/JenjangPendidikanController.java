package com.Jenjangpendidikan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.Jenjangpendidikan.repository.JenjangPendidikanRepository;



@Controller
@RequestMapping("/jenjangpendidikan/")
public class JenjangPendidikanController {

	@Autowired
	private JenjangPendidikanRepository jenjangPendidikanRepository;
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("jenjangpendidikan/indexapi");
		return view;
	}






}
