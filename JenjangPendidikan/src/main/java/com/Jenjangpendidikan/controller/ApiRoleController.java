package com.Jenjangpendidikan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.Jenjangpendidikan.model.Role;
import com.Jenjangpendidikan.repository.RoleRepository;



@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiRoleController {

	@Autowired
	public RoleRepository roleRepository;
	
	@GetMapping("role")
	public ResponseEntity<List<Role>> getRole() {
		try {
		List<Role> names = this.roleRepository.findByIsDelete(false);
		return new ResponseEntity<>(names, HttpStatus.OK);
	}catch(Exception e){
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
}