package com.Jenjangpendidikan.controller;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.Jenjangpendidikan.model.Pendaftaran;
import com.Jenjangpendidikan.repository.BiodataRepository;
import com.Jenjangpendidikan.repository.PendaftaranRepository;
import com.Jenjangpendidikan.repository.RoleRepository;
import com.Jenjangpendidikan.repository.TokenRepository;


@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiPendaftaranController {

	@Autowired
	public PendaftaranRepository pendaftaranRepository;

	@Autowired
	public BiodataRepository biodataRepository;

	@Autowired
	public TokenRepository tokenRepository;

	@Autowired
	public RoleRepository roleRepository;

	@GetMapping("getemail")
	public ResponseEntity<List<Pendaftaran>> getEmail(@RequestParam("email") String email) {
		List<Pendaftaran> emails = this.pendaftaranRepository.findByEmail(email);
		return new ResponseEntity<List<Pendaftaran>>(emails, HttpStatus.OK);
	}

	
	@PostMapping("register")
	public ResponseEntity<Object> pendaftaranss(@RequestBody Pendaftaran pendaftaran) {

		// String pass = passwordEncoder().encode(Pendaftaran.getPassword());
//	            pendaftaran.setEmail(pendaftarans.getEmail());
//	            pendaftaran.setPassword(pendaftarans.getPassword());;
		pendaftaran.setCreatedOn(new Date());
		Pendaftaran pendaftarans = this.pendaftaranRepository.save(pendaftaran);
		if (pendaftarans.equals(pendaftaran)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	@PutMapping("gantiPassword/{id}")
	public ResponseEntity<Object> gantipassword(@PathVariable("id") Long id, @RequestBody Pendaftaran pendaftaran) {
		Optional<Pendaftaran> pendaftarans = this.pendaftaranRepository.findById(id);

		if (pendaftarans.isPresent()) {
			pendaftaran.setEmail(pendaftarans.get().getEmail());
//			pendaftaran.setModifyBy("user");
			pendaftaran.setIsDelete(pendaftarans.get().getIsDelete());
			pendaftaran.setModifyOn(Date.from(Instant.now()));
//			pendaftaran.setCreatedBy();
			pendaftaran.setCreatedOn(pendaftarans.get().getCreatedOn());
//			pendaftaran.setId(id);
			this.pendaftaranRepository.save(pendaftaran);
			return new ResponseEntity<Object>("Update Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
}