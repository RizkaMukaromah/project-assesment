package com.Jenjangpendidikan.controller;

import java.math.BigInteger;
import java.security.PublicKey;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.Jenjangpendidikan.model.Biodata;
import com.Jenjangpendidikan.model.JenjangPendidikan;
import com.Jenjangpendidikan.repository.BiodataRepository;
import com.Jenjangpendidikan.repository.JenjangPendidikanRepository;
import com.Jenjangpendidikan.repository.PendaftaranRepository;
import com.Jenjangpendidikan.repository.RoleRepository;



@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiJenjangPendidikan {
	@Autowired
	public JenjangPendidikanRepository jenjangPendidikanRepository;
	

	@GetMapping("jenjangpendidikan")
	public ResponseEntity<List<JenjangPendidikan>> getAllJenjangPendidikan(){
		try {
			List<JenjangPendidikan> jenjangPendidikan = this.jenjangPendidikanRepository.findByIsDelete(false);
			return new ResponseEntity<>(jenjangPendidikan, HttpStatus.OK);
					
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT); 
		}
	}
	@GetMapping("name")
	public ResponseEntity<List<JenjangPendidikan>> getAllJenjangPendidikan(@RequestParam("name") String name) {
		List<JenjangPendidikan> names = this.jenjangPendidikanRepository.findByName(name);
		return new ResponseEntity<List<JenjangPendidikan>>(names,HttpStatus.OK);
	}
	
	@GetMapping("jenjangpendidikan/{id}")
	public ResponseEntity<List<JenjangPendidikan>> getJenjangPendidikanById(@PathVariable("id") Long id){
		try {
			Optional<JenjangPendidikan> jenjangPendidikan = this.jenjangPendidikanRepository.findById(id);
			if(jenjangPendidikan.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(jenjangPendidikan, HttpStatus.OK);
				return rest;
			}else {
				return ResponseEntity.notFound().build();
			}
		}catch(Exception e) {
			return new ResponseEntity<List<JenjangPendidikan>>(HttpStatus.NO_CONTENT);
		}
	}
	@PostMapping("add/jenjangpendidikan")
	public ResponseEntity<Object> saveJenjangPendidikan(@RequestBody JenjangPendidikan jenjangPendidikan){
		Long user = new Long("1");
		jenjangPendidikan.setCreatedBy(user);
		jenjangPendidikan.setIsDelete(false);
		jenjangPendidikan.setCreatedOn(Date.from(Instant.now()));
		//control untuk menympan data
		JenjangPendidikan jenjangPendidikans = this.jenjangPendidikanRepository.save(jenjangPendidikan);
		if(jenjangPendidikans.equals(jenjangPendidikan)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("edit/jenjangpendidikan/{id}")
	public ResponseEntity<Object> updateJenjangPendidikan(@PathVariable("id") Long id, @RequestBody JenjangPendidikan jenjangPendidikan) {
		Optional<JenjangPendidikan> jenjangPendidikans = this.jenjangPendidikanRepository.findById(id);

		if (jenjangPendidikans.isPresent()) {
			Long user = new Long("1");;
			jenjangPendidikan.setId(id);
			jenjangPendidikan.setModifyBy(user);
			jenjangPendidikan.setIsDelete(jenjangPendidikans.get().getIsDelete());
			jenjangPendidikan.setModifyOn(Date.from(Instant.now()));
			jenjangPendidikan.setCreatedBy(jenjangPendidikans.get().getCreatedBy());
			jenjangPendidikan.setCreatedOn(jenjangPendidikans.get().getCreatedOn());
			this.jenjangPendidikanRepository.save(jenjangPendidikan);
			return new ResponseEntity<Object>("Update Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	@PutMapping("delete/jenjangpendidikan/{id}")
	public ResponseEntity<Object> deleteJenjangPendidikan(@PathVariable("id") Long id) {
		Optional<JenjangPendidikan> jenjangPendidikans = this.jenjangPendidikanRepository.findById(id);

		if (jenjangPendidikans.isPresent()) {
			Long delete = new Long("1");
			JenjangPendidikan jenjangPendidikan = new JenjangPendidikan();
			jenjangPendidikan.setId(id);
			jenjangPendidikan.setIsDelete(true);
			jenjangPendidikan.setDeletedBy(delete);
			jenjangPendidikan.setDeletedOn(Date.from(Instant.now()));
			jenjangPendidikan.setCreatedBy(jenjangPendidikans.get().getCreatedBy());
			jenjangPendidikan.setCreatedOn(jenjangPendidikans.get().getCreatedOn());
			jenjangPendidikan.setModifyBy(jenjangPendidikans.get().getModifyBy());
			jenjangPendidikan.setModifyOn(jenjangPendidikans.get().getModifyOn());
			jenjangPendidikan.setName(jenjangPendidikans.get().getName());
			this.jenjangPendidikanRepository.save(jenjangPendidikan);
			return new ResponseEntity<>("Delete Successfully", HttpStatus.OK);

		} else {
			return ResponseEntity.notFound().build();
		}
	}
	@PostMapping("jenjangpendidikan/search/")
	public ResponseEntity<List<JenjangPendidikan>> getJenjangPendidikanName(@RequestParam("keyword") String keyword) {
		if (keyword.equals("")){
			List<JenjangPendidikan> jenjangPendidikan = this.jenjangPendidikanRepository.findByIsDelete(false);
			return new ResponseEntity<List<JenjangPendidikan>>(jenjangPendidikan, HttpStatus.OK);
		} else {
			List<JenjangPendidikan> jenjangPendidikan = this.jenjangPendidikanRepository.searchByKeyword(keyword);
			return new ResponseEntity<List<JenjangPendidikan>>(jenjangPendidikan, HttpStatus.OK);
		}
	}
//    @GetMapping("/searchjenjangpendidikan/{keyword}")
//    public ResponseEntity<List<JenjangPendidikan>> SearchJenjangPendidikanName(@PathVariable("keyword") String keyword)
//    {
//        if (keyword != null)
//        {
//            List<JenjangPendidikan> jenjangPendidikan = this.jenjangPendidikanRepo.SearchJenjangPendidikan(keyword);
//            return new ResponseEntity<>(jenjangPendidikan, HttpStatus.OK);
//        } else {
//            List<JenjangPendidikan> jenjangPendidikan = this.jenjangPendidikanRepo.findAll();
//            return new ResponseEntity<>(jenjangPendidikan, HttpStatus.OK);
//        }
//    }


    //get by input name
//    @GetMapping("/jenjangpendidikanname/{name}")
//    public ResponseEntity<List<JenjangPendidikan>> GetJenjangPendidikanByName(@PathVariable("name") String name) {
//        try {
//            //optional berarti data yang dicari bisa null
//            Optional<JenjangPendidikan> jenjangPendidikanOptional = this.jenjangPendidikanRepo.findByName(name);
//            if (jenjangPendidikanOptional.isPresent()) {
//                ResponseEntity rest = new ResponseEntity(jenjangPendidikanOptional, HttpStatus.OK);
//                return rest;
//            } else {
//                return ResponseEntity.notFound().build();
//            }
//        } catch (Exception ex) {
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        }
//    }

//    @PostMapping(value = "/jenjangpendidikan")
//    public ResponseEntity<Object> SaveJenjangPendidikan(@RequestBody JenjangPendidikan jenjangPendidikan, HttpServletRequest request) {
//        try {
//            long id = (long) request.getSession().getAttribute("id");
//            jenjangPendidikan.setCreatedBy(id);
//            jenjangPendidikan.setCreatedOn(new Date());
//            this.jenjangPendidikanRepo.save(jenjangPendidikan);
//            return new ResponseEntity<>("success", HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
//        }
//    }





	
}
