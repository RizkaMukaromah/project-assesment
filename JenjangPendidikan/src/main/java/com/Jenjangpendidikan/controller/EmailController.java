package com.Jenjangpendidikan.controller;
// Java Program to Create Rest Controller that
// Defines various API for Sending Mail



// Importing required classes
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.Jenjangpendidikan.model.Token;
import com.Jenjangpendidikan.service.EmailService;



// Annotation
@RestController
// Class
public class EmailController {

	@Autowired private EmailService emailService;

	// Sending a simple Email
	@PostMapping("/sendMail")
	public String
	sendMail(@RequestBody Token details)
	{
		String status
			= emailService.sendSimpleMail(details);

		return status;
	}

	// Sending email with attachment
}