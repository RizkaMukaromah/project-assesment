package com.Jenjangpendidikan.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.Jenjangpendidikan.model.Token;



@Service
public class EmailService {
	@Autowired 
	private JavaMailSender javamailsender;
	@Value("${spring.mail.username}") private String sender;
    
    public String sendSimpleMail(Token token)
    {
 
        // Try block to check for exceptions
        try {
 
            // Creating a simple mail message
            SimpleMailMessage mailMessage
                = new SimpleMailMessage();
 
            // di gunakan buat mengirim otp
            mailMessage.setFrom(sender);
            mailMessage.setTo(token.getEmail());
            mailMessage.setText("JANGAN SEBARKAN OTP INI ! OTP anda adalah :"+token.getToken());
            mailMessage.setSubject("OTP Registration Account");
 
            // Sending the mail
            javamailsender.send(mailMessage);
            return "Mail Sent Successfully...";
        }
 
        // Catch block to handle the exceptions
        catch (Exception e) {
            return "Error while Sending Mail";
        }
    }
}
