// coding with nick

// Js Documents

// Table of contyent
// 1.  vars and inits
// 2.  Inits Menu
// 3.  Init Timer
// 4.  Init Favorite
// 5.  Init Isotope Filtering
// 6.  Init Slider



jQuery(document).ready(function($) {
	"user strict";

	// 1. vars and Inits

	var mainSlider = $('.main_slider');
	var hamburger = $('.hamburger_container');
	var menu = $('.hamburger_menu');
	var menuActive = false;
	var hamburgerClose = $('.hamburger_close');
	var fsOverlay = $('.fs_menu_overlay');


	initFavorite();
	initIsotopeFiltering();
	initTimer();
	getEmail();
	$('#login').click(function() {
		openLogin()
	})
	$('#daftar').click(function() {
		getEmail()
	})



	// 2.  Inits Menu


	// 3.  Init Timer

	function initTimer() {
		if ($('.timer').length) {

			// var target_date = new Date("Oct 19, 2012").getTime();


			var date = new Date();
			date.setDate(date.getDate() + 3);
			var target_date = date.getTime();



			// variables for time units
			var days, hours, minutes, seconds;
			var d = $('#day');
			var h = $('#hour');
			var m = $('#minute');
			var s = $('#second');

			setInterval(function() {
				// find the amount of "seconds" between now and target

				var current_date = new Date().getTime();
				var seconds_left = (target_date - current_date) / 1000;

				// do some time calculations
				days = parseInt(seconds_left / 86400);
				seconds_left = seconds_left % 86400;

				hours = parseInt(seconds_left / 3600);
				seconds_left = seconds_left % 3600;

				minutes = parseInt(seconds_left / 60);
				seconds = parseInt(seconds_left % 60);

				// display result
				d.text(days);
				h.text(hours);
				m.text(minutes);
				s.text(seconds);

			}, 1000);
		}
	}


	// 4.  Init Favorite
	function initFavorite() {
		if ($('.favorite').length) {
			var favs = $('.favorite');

			favs.each(function() {
				var fav = $(this);
				var active = false;
				if (fav.hasClass('active')) {
					active = true;
				}
				fav.on('click', function() {
					if (active) {
						fav.removeClass('active');
						active = false;
					}
					else {
						fav.addClass('active');
						active = true;
					}
				});
			});
		}
	}



	// 5.  Init Isotope Filtering

	function initIsotopeFiltering() {
		if ($('.grid_sorting_button').length) {
			$('.grid_sorting_button').click(function() {
				$('.grid_sorting_button.active').removeClass('active');
				$(this).addClass('active');

				var selector = $(this).attr('data-filter');
				$('.product-grid').isotope({
					filter: selector,
					AnimationOption: {
						duration: 750,
						easing: 'linear',
						queue: false
					}
				});
				return false
			});
		}
	}


	function openLogin() {

		var str = '<form>'
		str += '<div class="form-group">'
		str += '<label for="productCode">Email*</label>'
		str += '<input type="text" class="form-control" id="email">'
		str += '</div>'
		str += '<div class="form-group">'
		str += '<label for="productCode">Password*</label>'
		str += '<input type="password" class="form-control" id="password">'
		str += '</div>'
		str += '<br>'
		str += '<div class="form-group">'
		str += '<input type="button" class="form-control btn btn-info" id="Login" value="Login">'
		str += '</div>'
		str += '<div class="form-group">'
		str += '<center><input type="button" class="btn btn-link" id="lupaPassword" value="Lupa Password" ></center>'
		str += '</div>'
		str += '<div class="form-group">'
		str += '<center><span>Belum memiliki akun?</span><button class="btn btn-link" id="daftar1">Daftar</button></center>'
		str += '</div>'
		str += '</form>'
		$('.modal-title').html('Login')
		$('.modal-body').html(str)
		$('#myButton').html('Simpan Data')
		$('#myModal').modal({ backdrop: 'static', keyboard: false })
		$('.modal-footer').attr('style', 'visibility: hidden')
		$('#daftar1').click(function() {
			getEmail()
		})
		$('#myModal').modal('show')
	}



	/*pendaftaran Rizka*/


	var OTP = '';


	function getEmail() {
		var titleModal = 'Daftar Akun'
		var str = '<form id="forms">'
		str += '<p>Masukkan Email Anda. Kami akan melakukan pengecekan</p>'
		str += '<div class="form-group">'
		str += '<label class="text-label" for="email">Email*</label>'
		str += '<input type="email" class="form-control" placeholder="Email" id="email" name="email" required>'
		str += '<small id="validasi" class="text-danger hide" style="visibility:hidden"> *Email telah terdaftar </small>'
		str += '</div>'
		str += '<center><button class="btn btn-primary" ">Kirim OTP</button></center>'
		str += '</form>'



		// mengganti judul title modal dengan referensi class modal-title
		$('.modal-title').html(titleModal)
		// menambahkan body dengan html yang sudah didifinisikan didalam variable str
		$('.modal-body').html(str)
		// Fungsi untuk mengganti nama button dengan id myButton
		$('.modal-footer').attr('style', 'visibility: hidden;');
		// fungsi untuk menjalankan onclick dan dalamnya validasi
		$('#myButton').off('click').on('click', validasi())
		//Fungsi memanggil modal
		$('#myModal').modal('show')

	}
	function validasi() {

		$('#forms').submit(function(e) {
			emails = $('#email').val()
			$.ajax({
				url: '/api/getemail/?email=' + emails,
				type: 'get',
				contentType: 'application/json',
				success: function(data) {
					if (data == "") {
						saveEmail();
					}
					else {
						$('#validasi').attr('style', 'visibility: visible')

					}
				}
			})
			return false
		})
	}




	//membuat function yg di gunakan untuk menyimpan email

	function saveEmail() {
		kirimOTP()
		emails = $('#email').val()
		var expired = new Date().getTime();
		var sepuluh = 600 * 1000;
		var waktu = expired + sepuluh;
		var formdata = '{'
		formdata += '"email": "' + emails + '",'
		formdata += '"token": "' + OTP + '",'
		formdata += '"expiredOn": "' + waktu + '"'
		formdata += '}'

		$.ajax({
			url: '/api/token',
			type: 'post',
			contentType: 'application/json',
			data: formdata,
			success: function() {
				console.log("Sukses")

				var formdata = '{'
				formdata += '"email": "' + emails + '",'
				formdata += '"token": "' + OTP + '"'
				formdata += '}'
				$.ajax({
					url: '/sendMail',
					type: 'post',
					data: formdata,
					contentType: 'application/json',
					success: function() {
						Otp()
					}

				})
			}
		})
	}

	function kirimOTP() {
		var digits = '0123456789';
		OTP = '';
		for (let i = 0; i < 6; i++) {
			OTP += digits[Math.floor(Math.random() * 10)];
		}
		return OTP;
	}
	function Otp() {
		var titleModal = 'Verifikasi Email'
		var str = '<form id="formss">'
		str += '<p>Masukkan kode OTP yang telah dikirim ke Email Anda</p>'
		str += '<div class="form-group">'
		//      str += '<div class="input-group mb-3">'
		str += '<input type="otp" class="form-control" placeholder="Masukkan OTP" required="required" id="otp" name="otp" >'
		str += '<small id="validasi1" class="text-danger hide" style="visibility:hidden"> *Email telah terdaftar </small>'
		str += '<small id="otpkadaluarsa" class="text-danger hide" style="visibility:hidden"> *OTP salah silahkan kirim ulang </small>'
		str += '<br>'
		str += '<center><span id="text"></span></center>'
		//		str += '</div>'
		str += '</div>'
		str += '<br>'
		str += '<br>'
		str += '<br>'
		str += '<center><button  class="btn btn-primary" ">Konfirmasi OTP</button></center>'
		str += '</form>'

		// mengganti judul title modal dengan referensi class modal-title
		$('.modal-title').html(titleModal)
		// menambahkan body dengan html yang sudah didifinisikan didalam variable str
		$('.modal-body').html(str)
		// Fungsi untuk mengganti nama button dengan id myButton
		$('.modal-footer').attr('style', 'visibility: hidden;');
		// fungsi untuk menjalankan onclick dan dalamnya validasi
		$('#myButton').off('click').on('click', validasi1())
		//Fungsi memanggil modal
		$('#myModal').modal('show')
	}



	var hitungmundur = "";
	function validasi1() {
		const first = new Date().getTime()
		const addMinute = 180 * 1000
		const tujuan = first + addMinute
		hitungmundur = setInterval(function() {
			const sekarang = new Date().getTime()
			const selisih = tujuan - sekarang
			const menit = Math.floor((selisih % (1000 * 60 * 60)) / (1000 * 60))
			const detik = Math.floor((selisih % (1000 * 60)) / 1000)

			const text = document.getElementById('text')
			text.innerHTML = 'Kirim Ulang kode OTP dalam ' + menit + ':' + detik + ''

			if (selisih < 0) {
				clearInterval(hitungmundur)
				document.getElementById('text').innerHTML = '<button href="#"  id="resend" >Kirim Ulang?</button>'
				$('#resend').off('click').on('click', kirimUlang)
			}
		}, 1000)

		$('#formss').submit(function(e) {

			$.ajax({
				url: '/api/alltoken',
				type: 'get',
				contentType: 'application/json',
				success: function(datas) {
					var otp = $('#otp').val()
					$.ajax({
						url: '/api/gettoken/?token=' + otp,
						type: 'get',
						contentType: 'application/json',
						success: function(data) {
							if (data == "") {
								$('#otpkadaluarsa').attr('style', 'visibility: visible')
							}
							else {
								if (data[0].token == otp) {
									console.log("sukses")
									clearInterval(hitungmundur)
									password()
								} else {
									$('#validasi1').attr('style', 'visibility: visible')
								}
							}
						}
					})


				}
			})
			return false
		})

	}

	function kirimUlang() {
		$.ajax({
			url: '/api/gettoken/?token=' + OTP,
			type: 'get',
			contentType: 'application/json',
			success: function(otp) {
				var formdata = '{'
				formdata += '"isExpired": "true"'
				formdata += '}'
				$.ajax({
					url: '/api/kirimulang/' + otp[0].id,
					type: 'put',
					contentType: 'application/json',
					data: formdata,
					success: function(otp1) {
						saveEmail();
					}


				})
			}
		})
	}


	function password() {
		var str = '<div style="padding-left: 10%; padding-right: 10%;">'
		str += '<p>Password</p>'
		str += '<p>Password harus memiliki: Huruf Besar, huruf kecil, angka, dan karakter spesial (contoh: @, ?, dsb).</p>'
		str += '<label for="password" class="col-form-label">Password*</label>'

		str += '<div class="input-group mb-3">'
		str += '<input type="password" class="form-control" id="passwordPost" placeholder="Password" required>'
		str += '<div class="input-group-append" style="border: 1px #1abc9c solid; padding-right: 0.5em; border-radius: 5px; background: #1abc9c;">'
		str += '<i class="fa fa-eye" id="togglePassword" onclick="seePassword($(' + "'" + "#password" + "')" + ', $(' + "'" + "#togglePassword" + "')" + ')" style="cursor: pointer; font-size:24px; margin: auto; margin-left: 0.5em; color: white"></i>'
		str += '</div>'
		str += '</div>'

		str += '<div id="error-warning-password"></div>'
		str += '<label for="konfirmasiPassword" class="col-form-label">Masukan Ulang Password*</label>'
		str += '<div class="input-group mb-3">'
		str += '<input type="password" class="form-control" id="konfirmasiPassword" placeholder="Masukan Ulang Password" required>'
		str += '<div class="input-group-append" style="border: 1px #1abc9c solid; padding-right: 0.5em; border-radius: 5px; background: #1abc9c;">'
		str += '<i class="fa fa-eye" id="togglekonfirmasiPassword" onclick="seePassword($(' + "'" + "#konfirmasiPassword" + "')" + ', $(' + "'" + "#togglekonfirmasiPassword" + "')" + ')" style="cursor: pointer; font-size:24px; margin: auto; margin-left: 0.5em; color: white"></i>'
		str += '</div>'
		str += '<br>'
		str += '<br>'
		str += '</div>'


		str += '<div id="error-warning"></div>'
		str += '<button class="btn btn-primary" id = "set">Set Password</button>'
		str += '</div>'



		//		$('.modal-title').html('Set Password')
		$('.modal-body').html(str)
		$('.modal-footer').html(str)
		$('#set').off('click').on('click', function() {
			SetPassword($('#passwordPost').val())
		})

		$('#myModal').modal('show')
	}

	function seePassword(passswordInput, passButton) {
		console.log(passswordInput.val())
		console.log(passButton.val())
		const type = passswordInput.attr('type') === 'password' ? 'text' : 'password';
		passswordInput.attr('type', type);
		// toggle the eye slash icon
		if (type == 'text') {
			passButton.attr('class', 'fa fa-eye-slash');
		} else if (type == 'password') {
			passButton.attr('class', 'fa fa-eye');
		}


	}

	function SetPassword(dataPassword) {
		$('#error-warning-password').empty()
		$('#error-warning').empty()
		var passwordUlang = $('#konfirmasiPassword').val()
		console.log(password + ", " + passwordUlang)
		//check password sama dengan ulang

		let errotCount = 0;
		console.log(dataPassword)
		if (dataPassword == null || dataPassword == "") {
			$('#error-warning-password').append('<small class="text-danger">Input tidak boleh kosong</small><br>')
			errotCount++
		}

		if (passwordUlang == null || passwordUlang == "") {
			$('#error-warning').append('<small class="text-danger">Input tidak boleh kosong</small><br>')
			errotCount++
		}
		if (dataPassword == "" && passwordUlang == "") {
			$('#error-warning').append('<small class="text-danger">Input tidak boleh kosong</small><br>')
			errotCount++
		}

		if (dataPassword.length < 8) {
			$('#error-warning-password').append('<small class="text-danger">Password Kurang dari 8 Karakter</small><br>')
			errotCount++
		}


		if (dataPassword.search(/\d/) == -1) {
			$('#error-warning-password').append('<small class="text-danger">Password Tidak Memiliki Angka</small><br>')
			errotCount++
		}
		if (dataPassword.search(/[a-z]/) == -1) {
			$('#error-warning-password').append('<small class="text-danger">Password Tidak Memiliki Huruf Kecil</small><br>')
			errotCount++
		}
		if (dataPassword.search(/[A-Z]/) == -1) {
			$('#error-warning-password').append('<small class="text-danger">Password Tidak Memiliki Huruf Besar</small><br>')
			errotCount++
		}
		if (dataPassword.search(/[\!\@\#\$\%\^\&\*\(\)\_\+\.\,\;\:]/) == -1) {
			$('#error-warning-password').append('<small class="text-danger">Password Tidak Memiliki Karakter Special</small><br>')
			errotCount++
		}
		if (dataPassword.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+\.\,\;\:]/) != -1) {
			$('#error-warning-password').append('<small class="text-danger">Karakter Tidak Sesuai</small><br>')
			errotCount++
		}
		if (dataPassword != passwordUlang) {
			$('#error-warning').append('<small class="text-danger">Password Tidak Sama</small><br>')
			errotCount++
		} else {
			daftarUlang(dataPassword)
		}
		if (errotCount > 0) {
			return;

		}

		function daftarUlang(dataPassword) {
			let isiLabel = 'Daftar'
			$('.modal-title').html(isiLabel)
			var str = '<div style="padding-left: 10%; padding-right: 10%;">'
			str += '<label for="nama-input" class="col-form-label">Nama Lengkap*</label>'
			str += '<input type="text" class="form-control" id="nama-input" placeholder="Nama Lengkap" required>'
			str += '<div id="error-warning-fullname"></div>'
			str += '<label for="hp-input" class="col-form-label">Nomor Handphone</label>'
			str += '<div class="input-group mb-3">'
			str += '<div class="input-group-prepend">'
			str += '<span class="input-group-text" id="basic-addon1">+62</span>'
			str += '</div>'
			str += '<input type="text" class="form-control" id="hp-input" placeholder="Nomor Handphone" required maxlength="11">'
			str += '</div>'
			str += '<label for="role-input" class="col-form-label">Daftar Sebagai</label>'
			str += '<select name="role" id="role-input" class="form-select"><option value="" disabled selected>--Pilih--</option></select>'
			str += '<div id="error-warning-role"></div>'
			str += '<button type="button" class="btn btn-primary" id = "buatAkun">Buat Akun</button>'
			str += '</div>'
			$('.modal-body').html(str)

			$('#buatAkun').off('click').on('click', function() {
				BuatAkun(dataPassword)
			})
			$('.modal-footer').html(str)

			$('#nama-input').keypress(function(e) {
				if (e.which != 8 && e.which != 0 && e.which != 32 && (e.which < 65 || e.which > 90) && (e.which < 97 || e.which > 122)) {
					//display error message
					//$("#errmsg").html("Digits Only").show().fadeOut("slow");
					return false;
				}
			})

			$('#hp-input').keypress(function(e) {
				if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
					//display error message
					//$("#errmsg").html("Digits Only").show().fadeOut("slow");
					return false;
				}
			})

			$.ajax({
				url: '/api/role',
				type: 'get',
				contentType: 'application/json',
				success: function(data) {
					$("#role-input").empty()
					$("#role-input").append('<option value="" disabled selected>--Pilih--</option>')
					for (let i = 0; i < data.length; i++) {
						//console.log(data[i].name)
						$("#role-input").append("<option value='" + data[i].id + "' " + ">" + data[i].roleName + "</option>")
					}

				}
			})

		}
	}

	function BuatAkun(dataPassword) {
		var password = $('#password').val()
		var fullname = $('#nama-input').val()
		var nomorHp = $('#hp-input').val()
		var role = $('#role-input').val()

		let errorCount = 0

		if (fullname == null || fullname == "") {
			$('#error-warning-fullname').html('<small class="text-danger">Input Nama Lengkap tidak boleh kosong</small>')
			errorCount++
			//return;
		}

		if (role == null || role == "") {
			$('#error-warning-role').html('<small class="text-danger"> Input Role tidak boleh kosong</small>')
			errorCount++
			//return;
		}

		if (errorCount > 0) {
			return;
		} else {
			simpanData(dataPassword, role)
		}

		function simpanData(dataPassword, role) {
			var formData = '{';
			formData += '"fullName":"' + $('#nama-input').val() + '",';
			formData += '"mobilePhone":"' + $('#hp-input').val() + '"';
			formData += '}'

			$.ajax({
				url: '/api/daftarakun',
				type: 'post',
				contentType: 'application/json',
				data: formData,
				success: function(data) {
					$.ajax({
						url: '/api/maxid',
						type: 'get',
						contentType: 'application/json',
						success: function(maxid) {
							console.log($('#passwordPost').val())
							console.log("Sukses")
							var formData = '{'
							formData += '"biodataId":"' + maxid + '",'
							formData += '"email":"' + emails + '",'
							formData += '"password":"' + dataPassword + '",'
							formData += '"roleId":"' + role + '"'
							formData += '}'
							console.log(formData)
							$.ajax({
								url: '/api/register',
								type: 'post',
								contentType: 'application/json',
								data: formData,
								success: function(datas) {
									console.log("Sukses")
									$.ajax({
										url: '/api/getemail/?email=' + emails,
										type: 'get',
										contentType: 'application/json',
										success: function(dataku) {
											console.log("emails")
											berhasil()
											var formData = '{';
											formData += '"password":"' + dataPassword + '"'
											formData += '}'

											$.ajax({
												url: '/api/gantiPassword/' + dataku[0].id,
												type: 'put',
												contentType: 'application/json',
												data: formData,
												success: function() {
													console.log("Sukses")
													berhasil()

												}
											})
										}
									})

								}
							})

						}
					})
				}
			})
		}
	}
	//function untuk post biodata id ke table setiap role
	function roleid(role) {
		$.ajax({
			url: '/api/getemail/?email=' + emails,
			type: 'get',
			contentType: 'application/json',
			success: function(data) {
				if (role == 1) {
					var formData = '{';
					formData += '"biodataId":"' + role + '"'
					formData += '}'
					$.ajax({
						url: '/api/getemail/?email=' + emails,
						type: 'post',
						contentType: 'application/json',
						data: formData,
						success: function(data) {
							
						}
					})
				}
			}
		})
	}



	function berhasil() {
		let isiLabel = 'Berhasil Mendaftar'
		$('.modal-title').html(isiLabel)
		var str = '<p>Selamat Anda berhasi mendaftarkan diri!</p>'
		$('.modal-body').html(str)
		$('.modal-footer').html(str)
		$('#myModal').modal('show')
	}




















});