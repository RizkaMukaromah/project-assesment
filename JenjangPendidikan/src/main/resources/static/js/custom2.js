// coding with nick

// Js Documents

// Table of contyent
// 1.  vars and inits
// 2.  Inits Menu
// 3.  Init Timer
// 4.  Init Favorite
// 5.  Init Isotope Filtering
// 6.  Init Slider



jQuery(document).ready(function($) {
	"user strict";

	// 1. vars and Inits

	var mainSlider = $('.main_slider');
	var hamburger = $('.hamburger_container');
	var menu = $('.hamburger_menu');
	var menuActive = false;
	var hamburgerClose = $('.hamburger_close');
	var fsOverlay = $('.fs_menu_overlay');


	initFavorite();
	initIsotopeFiltering();
	initTimer();

	$('#login').click(function() {
		openLogin()
	})





	// 2.  Inits Menu


	// 3.  Init Timer

	function initTimer() {
		if ($('.timer').length) {

			// var target_date = new Date("Oct 19, 2012").getTime();


			var date = new Date();
			date.setDate(date.getDate() + 3);
			var target_date = date.getTime();



			// variables for time units
			var days, hours, minutes, seconds;
			var d = $('#day');
			var h = $('#hour');
			var m = $('#minute');
			var s = $('#second');

			setInterval(function() {
				// find the amount of "seconds" between now and target

				var current_date = new Date().getTime();
				var seconds_left = (target_date - current_date) / 1000;

				// do some time calculations
				days = parseInt(seconds_left / 86400);
				seconds_left = seconds_left % 86400;

				hours = parseInt(seconds_left / 3600);
				seconds_left = seconds_left % 3600;

				minutes = parseInt(seconds_left / 60);
				seconds = parseInt(seconds_left % 60);

				// display result
				d.text(days);
				h.text(hours);
				m.text(minutes);
				s.text(seconds);

			}, 1000);
		}
	}


	// 4.  Init Favorite
	function initFavorite() {
		if ($('.favorite').length) {
			var favs = $('.favorite');

			favs.each(function() {
				var fav = $(this);
				var active = false;
				if (fav.hasClass('active')) {
					active = true;
				}
				fav.on('click', function() {
					if (active) {
						fav.removeClass('active');
						active = false;
					}
					else {
						fav.addClass('active');
						active = true;
					}
				});
			});
		}
	}



	// 5.  Init Isotope Filtering

	function initIsotopeFiltering() {
		if ($('.grid_sorting_button').length) {
			$('.grid_sorting_button').click(function() {
				$('.grid_sorting_button.active').removeClass('active');
				$(this).addClass('active');

				var selector = $(this).attr('data-filter');
				$('.product-grid').isotope({
					filter: selector,
					AnimationOption: {
						duration: 750,
						easing: 'linear',
						queue: false
					}
				});
				return false
			});
		}
	}


	function openLogin() {

		var str = '<form>'
		str += '<div class="form-group">'
		str += '<label for="productCode">Email*</label>'
		str += '<input type="text" class="form-control" id="email">'
		str += '</div>'
		str += '<div class="form-group">'
		str += '<label for="productCode">Password*</label>'
		str += '<input type="password" class="form-control" id="password">'
		str += '</div>'
		str += '<br>'
		str += '<div class="form-group">'
		str += '<input type="button" class="form-control btn btn-info" id="Login" value="Login">'
		str += '</div>'
		str += '<div class="form-group">'
		str += '<center><input type="button" class="btn btn-link" id="lupaPassword" value="Lupa Password" ></center>'
		str += '</div>'
		str += '<div class="form-group">'
		str += '<center><span>Belum memiliki akun?</span><button class="btn btn-link" id="daftar1">Daftar</button></center>'
		str += '</div>'
		str += '</form>'
		$('.modal-title').html('Login')
		$('.modal-body').html(str)
		$('#myButton').html('Simpan Data')
		$('#myModal').modal({ backdrop: 'static', keyboard: false })
		$('.modal-footer').attr('style', 'visibility: hidden')
		$('#lupaPassword').click(function() {
			lupaPassword()
		})
		$('#myModal').modal('show')
	}



	/*pendaftaran Rizka*/

	var OTP = '';


	function lupaPassword() {
		var titleModal = 'Lupa Password'
		var str = '<form id="forms">'
		str += '<p>Masukkan Email Anda. Kami akan melakukan pengecekan</p>'
		str += '<div class="form-group">'
		str += '<label class="text-label" for="email">Email*</label>'
		str += '<input type="email" class="form-control" placeholder="Email" id="email" name="email" required>'
		str += '<small id="cek" class="text-danger hide" style="visibility:hidden"> *Email belum terdaftar </small>'
		str += '</div>'
		str += '<center><button class="btn btn-primary" ">Kirim OTP</button></center>'
		str += '</form>'



		// mengganti judul title modal dengan referensi class modal-title
		$('.modal-title').html(titleModal)
		// menambahkan body dengan html yang sudah didifinisikan didalam variable str
		$('.modal-body').html(str)
		// Fungsi untuk mengganti nama button dengan id myButton
		$('.modal-footer').attr('style', 'visibility: hidden;');
		// fungsi untuk menjalankan onclick dan dalamnya validasi
		$('#myButton').off('click').on('click', validation())
		//Fungsi memanggil modal
		$('#myModal').modal('show')

	}
	var emailss= ''
	function validation() {

		$('#forms').submit(function(e) {
			emails = $('#email').val()
			emailss=emails
			$.ajax({
				url: '/api/getemail/?email=' + emails,
				type: 'get',
				contentType: 'application/json',
				success: function(data) {
					if (data == "") {
						$('#cek').attr('style', 'visibility: visible')
					}
					else {
						$('#cek').attr('style', 'visibility: hidden')
						simpanEmail()

					}
				}
			})
			return false
		})
	}




	//membuat function yg di gunakan untuk menyimpan email

	function simpanEmail() {
		kirimOTP()
		emails = $('#email').val()
		var expired = new Date().getTime();
		var sepuluh = 600 * 1000;
		var waktu = expired + sepuluh;
		var formdata = '{'
		formdata += '"email": "' + emails + '",'
		formdata += '"token": "' + OTP + '",'
		formdata += '"expiredOn": "' + waktu + '"'
		formdata += '}'

		$.ajax({
			url: '/api/token',
			type: 'post',
			contentType: 'application/json',
			data: formdata,
			success: function() {
				console.log("Sukses")

				var formdata = '{'
				formdata += '"email": "' + emails + '",'
				formdata += '"token": "' + OTP + '"'
				formdata += '}'
				$.ajax({
					url: '/sendMail',
					type: 'post',
					data: formdata,
					contentType: 'application/json',
					success: function() {
						Otp()
					}

				})
			}
		})
	}

	function kirimOTP() {
		var digits = '0123456789';
		OTP = '';
		for (let i = 0; i < 6; i++) {
			OTP += digits[Math.floor(Math.random() * 10)];
		}
		return OTP;
	}
	function Otp() {
		var titleModal = 'Verifikasi Email'
		var str = '<form id="formss">'
		str += '<p>Masukkan kode OTP yang telah dikirim ke Email Anda</p>'
		str += '<div class="form-group">'
		//      str += '<div class="input-group mb-3">'
		str += '<input type="otp" class="form-control" placeholder="Masukkan OTP" required="required" id="otp" name="otp" >'
		str += '<small id="validasi1" class="text-danger hide" style="visibility:hidden"> *Email telah terdaftar </small>'
		str += '<small id="otpkadaluarsa" class="text-danger hide" style="visibility:hidden"> *OTP kadaluarsa silahkan kirim ulang </small>'
		str += '<br>'
		str += '<center><span id="text"></span></center>'
		//		str += '</div>'
		str += '</div>'
		str += '<br>'
		str += '<br>'
		str += '<br>'
		str += '<center><button  class="btn btn-primary" ">Konfirmasi OTP</button></center>'
		str += '</form>'

		// mengganti judul title modal dengan referensi class modal-title
		$('.modal-title').html(titleModal)
		// menambahkan body dengan html yang sudah didifinisikan didalam variable str
		$('.modal-body').html(str)
		// Fungsi untuk mengganti nama button dengan id myButton
		$('.modal-footer').attr('style', 'visibility: hidden;');
		// fungsi untuk menjalankan onclick dan dalamnya validasi
		$('#myButton').off('click').on('click', validasi1())
		//Fungsi memanggil modal
		$('#myModal').modal('show')
	}



	var hitungmundur = "";
	function validasi1() {
		const first = new Date().getTime()
		const addMinute = 180 * 1000
		const tujuan = first + addMinute
		hitungmundur = setInterval(function() {
			const sekarang = new Date().getTime()
			const selisih = tujuan - sekarang
			const menit = Math.floor((selisih % (1000 * 60 * 60)) / (1000 * 60))
			const detik = Math.floor((selisih % (1000 * 60)) / 1000)

			const text = document.getElementById('text')
			text.innerHTML = 'Kirim Ulang kode OTP dalam ' + menit + ':' + detik + ''

			if (selisih < 0) {
				clearInterval(hitungmundur)
				document.getElementById('text').innerHTML = '<button href="#"  id="resend" >Kirim Ulang?</button>'
				$('#resend').off('click').on('click', kirimUlang)
			}
		}, 1000)

		$('#formss').submit(function(e) {

			$.ajax({
				url: '/api/alltoken',
				type: 'get',
				contentType: 'application/json',
				success: function(datas) {
					var otp = $('#otp').val()
					$.ajax({
						url: '/api/gettoken/?token=' + otp,
						type: 'get',
						contentType: 'application/json',
						success: function(data) {
							if (data == "") {
								$('#otpkadaluarsa').attr('style', 'visibility: visible')
							}
							else {
								if (data[0].token == otp) {
									console.log("sukses")
									clearInterval(hitungmundur)
									password()
								} else {
									$('#validasi1').attr('style', 'visibility: visible')
								}
							}
						}
					})


				}
			})
			return false
		})

	}

	function kirimUlang() {
		$.ajax({
			url: '/api/gettoken/?token=' + OTP,
			type: 'get',
			contentType: 'application/json',
			success: function(otp) {
				var formdata = '{'
				formdata += '"isExpired": "true"'
				formdata += '}'
				$.ajax({
					url: '/api/kirimulang/' + otp[0].id,
					type: 'put',
					contentType: 'application/json',
					data: formdata,
					success: function(otp1) {
						simpanEmail();
					}


				})
			}
		})
	}


	function password() {
		var str = '<div style="padding-left: 10%; padding-right: 10%;">'
		str += '<p>Password</p>'
		str += '<p>Password harus memiliki: Huruf Besar, huruf kecil, angka, dan karakter spesial (contoh: @, ?, dsb).</p>'
		str += '<label for="password" class="col-form-label">Password*</label>'

		str += '<div class="input-group mb-3">'
		str += '<input type="password" class="form-control" id="passwordPost" placeholder="Password" required>'
		str += '<div class="input-group-append" style="border: 1px #1abc9c solid; padding-right: 0.5em; border-radius: 5px; background: #1abc9c;">'
		str += '<i class="fa fa-eye" id="togglePassword" onclick="seePassword($(' + "'" + "#password" + "')" + ', $(' + "'" + "#togglePassword" + "')" + ')" style="cursor: pointer; font-size:24px; margin: auto; margin-left: 0.5em; color: white"></i>'
		str += '</div>'
		str += '</div>'
		str += '<div id="error-warning-password"></div>'


		str += '<label for="konfirmasiPassword" class="col-form-label">Masukan Ulang Password*</label>'
		str += '<div class="input-group mb-3">'
		str += '<input type="password" class="form-control" id="konfirmasiPassword" placeholder="Masukan Ulang Password" required>'
		str += '<div class="input-group-append" style="border: 1px #1abc9c solid; padding-right: 0.5em; border-radius: 5px; background: #1abc9c;">'
		str += '<i class="fa fa-eye" id="togglekonfirmasiPassword" onclick="seePassword($(' + "'" + "#konfirmasiPassword" + "')" + ', $(' + "'" + "#togglekonfirmasiPassword" + "')" + ')" style="cursor: pointer; font-size:24px; margin: auto; margin-left: 0.5em; color: white"></i>'
		str += '</div>'
		str += '<br>'
		str += '<br>'
		str += '</div>'


		str += '<div id="error-warning"></div>'
		str += '<button class="btn btn-primary" id = "set">Set Password</button>'
		str += '</div>'



		//		$('.modal-title').html('Set Password')
		$('.modal-body').html(str)
		$('.modal-footer').html(str)
		$('#set').off('click').on('click', function() {
			SetPassword($('#passwordPost').val())
		})

		$('#myModal').modal('show')
	}

	function seePassword(passswordInput, passButton) {
		console.log(passswordInput.val())
		console.log(passButton.val())
		const type = passswordInput.attr('type') === 'password' ? 'text' : 'password';
		passswordInput.attr('type', type);
		// toggle the eye slash icon
		if (type == 'text') {
			passButton.attr('class', 'fa fa-eye-slash');
		} else if (type == 'password') {
			passButton.attr('class', 'fa fa-eye');
		}


	}

	function SetPassword(dataPassword) {
		$('#error-warning-password').empty()
		$('#error-warning').empty()

		password = $('#passwordPost').val()
		var passwordUlang = $('#konfirmasiPassword').val()
		//consolee.log(password+", "+passwordUlang)
		//check password sama dengan ulang

		let errotCount = 0;

		if (password == null || password == "") {
			$('#error-warning-password').append('<small class="text-danger">Input tidak boleh kosong</small><br>')
			errotCount++
		}

		if (passwordUlang == null || passwordUlang == "") {
			$('#error-warning').append('<small class="text-danger">Input tidak boleh kosong</small><br>')
			errotCount++
		}

		if (password.length < 8) {
			$('#error-warning-password').append('<small class="text-danger">Password Kurang dari 8 Karakter</small><br>')
			errotCount++
		}


		if (password.search(/\d/) == -1) {
			$('#error-warning-password').append('<small class="text-danger">Password Tidak Memiliki Angka</small><br>')
			errotCount++
		}
		if (password.search(/[a-z]/) == -1) {
			$('#error-warning-password').append('<small class="text-danger">Password Tidak Memiliki Huruf Kecil</small><br>')
			errotCount++
		}
		if (password.search(/[A-Z]/) == -1) {
			$('#error-warning-password').append('<small class="text-danger">Password Tidak Memiliki Huruf Besar</small><br>')
			errotCount++
		}
		if (password.search(/[\!\@\#\$\%\^\&\*\(\)\_\+\.\,\;\:]/) == -1) {
			$('#error-warning-password').append('<small class="text-danger">Password Tidak Memiliki Karakter Special</small><br>')
			errotCount++
		}
		if (password.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+\.\,\;\:]/) != -1) {
			$('#error-warning-password').append('<small class="text-danger">Karakter Tidak Sesuai</small><br>')
			errotCount++
		}
		if (password != passwordUlang) {
			$('#error-warning').append('<small class="text-danger">Password Tidak Sama</small><br>')
			errotCount++
		} else {
			simpanData(dataPassword)
		}
		if (errotCount > 0) {
			return;

		}

		function simpanData(dataPassword) {
			console.log(emailss)
			$.ajax({
				url: '/api/getemail/?email=' + emailss,
				type: 'get',
				contentType: 'application/json',
				success: function(data) {
					console.log("emails")
					berhasil()
					var formData = '{';
					formData += '"password":"' + dataPassword + '"'
					formData += '}'

					$.ajax({
						url: '/api/gantiPassword/' + data[0].id,
						type: 'put',
						contentType: 'application/json',
						data: formData,
						success: function() {
							console.log("Sukses")
							berhasil()
						}
					})
				}
			})
		}



		function berhasil() {
			let isiLabel = 'Selamat'
			$('.modal-title').html(isiLabel)
			var str = '<p>Selamat Anda berhasil merubah password!</p>'
			$('.modal-body').html(str)
			$('.modal-footer').html(str)
			$('#myModal').modal('show')
		}


	}



});